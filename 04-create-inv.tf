data "template_file" "vm" {
  template = file("${path.module}/templates/inventory.tpl")
  vars = {
    vm1 = join("\n", formatlist("%s ansible_ssh_host=%s ansible_ssh_user=cloud-user", yandex_compute_instance.vm1.*.name, yandex_compute_instance.vm1.*.network_interface.0.nat_ip_address))
    vm2 = join("\n", formatlist("%s ansible_ssh_host=%s ansible_ssh_user=ubuntu", yandex_compute_instance.vm2.*.name, yandex_compute_instance.vm2.*.network_interface.0.nat_ip_address))
    vm3 = join("\n", formatlist("%s ansible_ssh_host=%s ansible_ssh_user=ubuntu", yandex_compute_instance.vm3.*.name, yandex_compute_instance.vm3.*.network_interface.0.nat_ip_address))

  }

}

resource "local_file" "vm_file" {
  content  = data.template_file.vm.rendered
  filename = "${path.module}/ansible/inventory.ini"
}
