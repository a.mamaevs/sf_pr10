resource "yandex_compute_instance" "vm1" {

  name = "vm1"

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      size     = 20
      image_id = "fd8okkglfo3rhabhki0a"
    }
  }

  network_interface {
    subnet_id  = yandex_vpc_subnet.subnet-1.id
    ip_address = "192.168.10.17"
    nat        = true
  }

  metadata = {
    ssh-keys = "cloud-user:${file(var.ssh_key_public)}"
  }
}

resource "yandex_compute_instance" "vm2" {

  name = "vm2"

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      size     = 20
      image_id = "fd8qqvji2rs2lehr7d1l"
    }
  }

  network_interface {
    subnet_id  = yandex_vpc_subnet.subnet-1.id
    ip_address = "192.168.10.15"
    nat        = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file(var.ssh_key_public)}"
  }
}

resource "yandex_compute_instance" "vm3" {

  name = "vm3"

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      size     = 20
      image_id = "fd8qqvji2rs2lehr7d1l"
    }
  }

  network_interface {
    subnet_id  = yandex_vpc_subnet.subnet-1.id
    ip_address = "192.168.10.16"
    nat        = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file(var.ssh_key_public)}"
  }
}

resource "yandex_vpc_network" "network-1" {
  name = "network1"
}

resource "yandex_vpc_subnet" "subnet-1" {
  name           = "subnet1"
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.network-1.id
  v4_cidr_blocks = ["192.168.10.0/24"]
}